<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="baru.css">
</head>

<body style="width: 100%; height:100%;">
	<div class="wrap" style="width: 100%; height:100%;">
		<div class="header">
			<img src="fotoku.jpg" width="120" style="float: left;">
			<h3 style="height: 5px; color: white " class="judul">WEBSITE PROFILE</h3>
			<h4 class="judul">Ini Adalah Website Profile Maulana Husaini</h4>
		</div>
		<div class="menu" style="width: 100%; height:100%;">
			<ul>
				<li><a href="index.php">Home</a></li>

				<li><a href="biodata.php">Biodata</a></li>
				<li><a href="visimisi.php">Visi & Misi</a></li>
				<li><a href="buku.php">Buku Favorit</a></li>
				<li><a href="pencarian.html">pencarian</a></li>
				<li><a href="Hadiah.php">Hadiah</a></li>
				<li><a href="daftar.php">Daftar</a></li>
			</ul>
		</div>
		<div class="badan">
			<div class="sidebar">
				sidebar
				<h2>Find Me On : </h2>
				<ul>
					<a href="https://www.instagram.com/bandararembele/"><img src="ig.png" border="" height="50" width="50" style="position: absolute;"></a>
					<p style="position: absolute; margin-left: 50px;width: 100px; ">@maulana_husaini</p>

					<a href="https://www.facebook.com/rembeleairport/"><img src="fb2.png" border="" height="50" width="50" style="position: absolute; margin-top: 50px"></a>
					<p style="position: absolute; margin-left: 50px; margin-top: 70px; width: 100px; ">@MaulanaHusaini</p>

			</div>

			<div class="content">
				<title>BIODATA DIRI</title>


				<h1 align="center">BIODATA DIRI</h1>
				<table width="745" border="1" cellspacing="0" cellpadding="5" align="center">
					<tr align="center" bgcolor="#66CC33">
						<td width="174">DATA DIRI</td>
						<td width="353">KETERANGAN</td>
						<td width="232">FOTO</td>
					</tr>
					<tr>
						<td>Nama</td>
						<td>Maulana Husaini</td>
						<td rowspan="10" align="center"><img src="fotoku.jpg" width="210" height="313"></td>
					</tr>
					<tr>
						<td>Jurusan</td>
						<td>Teknik Informatika (TI)</td>
					</tr>
					<tr>
						<td>Jenjang</td>
						<td>Strata-1 (S1)</td>
					</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>Laki-laki</td>
					</tr>
					<tr>
						<td>Tempat/Tanggal Lahir</td>
						<td>Aceh, 24 Maret 1999</td>
					</tr>
					<tr>
						<td>Perguruan Tinggi</td>
						<td>Universitas Ahmad Dahlan</td>
					</tr>
					<tr>
						<td>Pekerjaan</td>
						<td>Pengangguran</td>
					</tr>
					<tr>
						<td>Hobi</td>
						<td>Ngoding</td>
					</tr>
				</table>


			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
			&copy;Maulana Husaini
		</div>
	</div>
</body>

</html>