<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="baru.css">
</head>

<body>
	<div class="wrap">
		<div class="header">
			<img src="fotoku.jpg" width="120" style="float: left;">
			<h3 style="height: 5px; color: white " class="judul">WEBSITE PROFILE</h3>
			<h4>Ini Adalah Website Profile Maulana Husaini</h4>
		</div>
		<div class="menu">
			<ul>
				<li><a href="index.php">Home</a></li>

				<li><a href="biodata.php">Biodata</a></li>

				<li><a href="visimisi.php">Visi & Misi</a></li>
				<li><a href="buku.php">Buku Favorit</a></li>
				<li><a href="pencarian.html">pencarian</a></li>
				<li><a href="Hadiah.php">Hadiah</a></li>
				<li><a href="daftar.php">Daftar</a></li>

			</ul>
		</div>
		<div class="badan">
			<div class="sidebar">
				<h2>Find Me On : </h2>
				<ul>
					<a href="https://www.instagram.com/bandararembele/"><img src="ig.png" border="" height="50" width="50" style="position: absolute;"></a>
					<p style="position: absolute; margin-left: 50px;width: 100px; ">@maulana_husaini</p>

					<a href="https://www.facebook.com/rembeleairport/"><img src="fb2.png" border="" height="50" width="50" style="position: absolute; margin-top: 50px"></a>
					<p style="position: absolute; margin-left: 50px; margin-top: 70px; width: 100px; ">@MaulanaHusaini</p>

			</div>
			<div class="content">
				<center>


					<center>
						<h2>SILAHKAN MENDAFTAR</h2>
					</center>
					<div class="login">
						<form action="#" method="POST" onSubmit="validasi()">
							<div>
								<label>Nama Lengkap:</label>
								<input type="text" name="nama" id="nama" />
							</div>
							<div>
								<label>Email:</label>
								<input type="email" name="email" id="email" />
							</div>
							<div>
								<label>Alamat:</label>
								<textarea cols="40" rows="5" name="alamat" id="alamat"></textarea>
							</div>
							<div>
								<input type="submit" value="Daftar" class="tombol">
							</div>
						</form>
					</div>

					<script type="text/javascript">
						function validasi() {
							var nama = document.getElementById("nama").value;
							var email = document.getElementById("email").value;
							var alamat = document.getElementById("alamat").value;
							if (nama != "" && email != "" && alamat != "") {
								return true;
							} else {
								alert('Anda harus mengisi data dengan lengkap !');
							}
						}
					</script>

			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
			&copy; 1800018133 - Maulana Husaini - Teknik Infromatika - Universitas Ahmad Dahlan
		</div>
	</div>
</body>

</html>